## En live CD arch

### en console

loadkeys fr_latin1
passwd
systemctl start sshd

### en ssh

ssh root@ip

cryptsetup open --type plain -d /dev/urandom /dev/sda to_be_wiped
dd if=/dev/zero of=/dev/mapper/to_be_wiped status=progress
cryptsetup close to_be_wiped
sync

sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | fdisk /dev/sda
g
n
1

+1G
t
1
n
2



p
w
q
EOF

mkfs.fat -F32 /dev/sda1
cryptsetup -h sha512 -i 5000 -v luksFormat --type luks2 /dev/sda2
cryptsetup open /dev/sda2 cryptroot

pvcreate /dev/mapper/cryptroot
vgcreate vg_root /dev/mapper/cryptroot
lvcreate -L 8g -n lv_swap vg_root
lvcreate -l 100%FREE -n lv_root vg_root

mkfs.ext4 /dev/mapper/vg_root-lv_root
mount /dev/mapper/vg_root-lv_root /mnt
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot
mkswap /dev/mapper/vg_root-lv_swap
swapon /dev/mapper/vg_root-lv_swap

timedatectl set-ntp true


curl -s "https://www.archlinux.org/mirrorlist/?country=FR&protocol=https&use_mirror_status=on" | sed -e 's/^#Server/Server/' -e '/^#/d' > /etc/pacman.d/mirrorlist
pacman -Sy pacman-contrib
curl -s "https://www.archlinux.org/mirrorlist/?country=FR&protocol=https&use_mirror_status=on" | sed -e 's/^#Server/Server/' -e '/^#/d' | rankmirrors -n 6 - > /etc/pacman.d/mirrorlist

pacstrap /mnt base base-devel 
pacstrap /mnt zip unzip p7zip vim mc alsa-utils syslog-ng mtools dosfstools lsb-release ntfs-3g exfat-utils

genfstab -U -p /mnt >> /mnt/etc/fstab

arch-chroot /mnt

## Dans le chroot : 

ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
timedatectl set-ntp true
hwclock --systohc --utc

cat > /etc/vconsole.conf  <<EOF
KEYMAP=fr-latin9
FONT=lat9w-16
EOF
cat > /etc/locale.conf <<EOF
LANG=fr_FR.UTF-8
LC_COLLATE=C
EOF
sed -i '/fr_FR.UTF-8/s/^#//g' /etc/locale.gen
export LANG=fr_FR.UTF-8
locale-gen

echo archlinux-test01 > /etc/hostname
echo "127.0.0.1          archlinux-test01" >> /etc/hosts
echo "::1                archlinux-test01" >> /etc/hosts
echo "127.0.1.1          archlinux-test01.localdomain archlinux-test01" >> /etc/hosts

bootctl --path=/boot install
cat > /boot/loader/loader.conf <<EOF
default  arch
timeout  0
editor   0
EOF
cat > /boot/loader/entries/arch.conf <<EOF
title   Arch Linux
linux   /vmlinuz-linux
initrd  /initramfs-linux.img
options cryptdevice=UUID=$(blkid -s UUID -o value /dev/sda2):cryptroot ro root=/dev/mapper/vg_root-lv_root rw quiet splash
EOF
sed -i "/^HOOKS=/c\HOOKS=(base udev autodetect modconf block keyboard keymap encrypt lvm2 fsck filesystems)" /etc/mkinitcpio.conf


useradd -m -g wheel -c 'Henri Nougayrede' -s /bin/bash henri
echo "henri:bonjour" | chpasswd
echo "root:bonjour" | chpasswd
echo "%wheel ALL=(ALL) ALL" > /etc/sudoers.d/g_wheel
pacman -S --noconfirm lvm2 networkmanager htop openssh sudo weston plasma kde-applications plasma-wayland-session gnome-keyring cronie xf86-video-vesa pamac
systemctl enable NetworkManager 
systemctl enable sddm
systemctl enable syslog-ng@default

pacman -S --noconfirm firefox-i18n-fr chromium libreoffice-still-fr hunspell-fr ttf-dejavu artwiz-fonts keepass xdotool ufw terminator


pacman -S --noconfirm virtualbox-guest-utils
systemctl enable vboxservice

sed -i -e 's/#ForwardToSyslog=no/ForwardToSyslog=yes/g' /etc/systemd/journald.conf

mkinitcpio -p linux

## Sortie du chroot

exit
umount -R /mnt
reboot

## Après le reboot

sudo localectl set-x11-keymap fr
sudo localectl set-keymap fr
sudo ufw enable
sudo paperconfig -p a4





